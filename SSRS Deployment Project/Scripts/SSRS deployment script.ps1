﻿###############################################################################################################################
######################---------- LOGIN TO AZURE ACCOUNT --------------------##############################

#$Username = "umer.riazt@cloud.weir"
#$Password = ConvertTo-SecureString "Nedia@1214" -AsPlainText -Force
#$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Username, $Password
#Login-AzureRmAccount -Credential $cred

######################---------- SELECT SUBSCRIPTION & CONTEXT--------------------##############################

#$sandbox=Get-AzureRmSubscription -SubscriptionId "93fc7ec5-ec4b-46bc-a07f-a5c4310c60ed"
#Set-AzureRmContext -SubscriptionObject $sandbox

######################------------------ SETTING UP VARIABLES --------------------##############################

Param(
	[parameter(Mandatory=$true)]
	[string] $rootPath
)

$location = "South Central US"
$resourceGroupPattern = 'us153fq-resg'
$automationAccountPattern= 'us153fq-aut'
$storageAccountPattern = 'us153fdstore'
$VmNamePattern = 'us153fq-winvm'
$prePath="\SQL Server Reporting Services Deployment-CI\drop"
$templatePath = $rootPath + $prePath+ "\SSRS Deployment Project\Templates\sqlReportingRMTemplate.json"
$parametersPath = $rootPath + $prePath+ "\SSRS Deployment Project\Templates\sqlReportingRMTemplate.parameters.Prod.json"
$cSqlPath = $rootPath + $prePath+ "\SSRS Deployment Project\Modules\cSql.zip"
$xNetworkingPath = $rootPath + $prePath+ "\SSRS Deployment Project\Modules\xNetworking.zip"
$xPSDesiredStateConfigurationPath = $rootPath + $prePath+ "\SSRS Deployment Project\Modules\xPSDesiredStateConfiguration.zip"
$urlShortnerPath = $rootPath + $prePath+ "\SSRS Deployment Project\Applications\Weir.Synertrex.UrlShortener.zip"
$SsrsPath = $rootPath + $prePath+ "\SSRS Deployment Project\Scripts\Ssrs.ps1"

$json = Get-Content $parametersPath | Out-String | ConvertFrom-Json
write-output $json.parameters

#############################################################################################################################
###############################-------------- RESOURCE GROUP ----------------##############################

Write-Output "************************ EXECUTING Resource Group ****************************"

if ($json.parameters.resourceGroup.value -eq '')
{
    #Following block gets last created RESOURCE GROUP of mentioned pattern and creates the new RESOURCE GROUP with new number

    Write-Output "Resource Group not found in parameters, creating a new Resource Group"     

    $lastCreatedResourceGroup = Get-AzureRmResourceGroup | Select-Object -ExpandProperty 'ResourceGroupName' | Select-String -Pattern $resourceGroupPattern
    
    if(($lastCreatedResourceGroup.GetType() |Select-Object -ExpandProperty 'Name') -eq  'String')
        {$lastCreatedResourceGroup = $lastCreatedResourceGroup.ToString()}
    else
        {$lastCreatedResourceGroup = ($lastCreatedResourceGroup| Select-Object -Last 1).ToString()}
        
    $lastCreatedResourceGroupNumber = [int32]$lastCreatedResourceGroup.Replace($resourceGroupPattern,'')
    $newResourceGroupNumber = $lastCreatedResourceGroupNumber + 1
    $newResourceGroup = $resourceGroupPattern + $newResourceGroupNumber.ToString()
    
    New-AzureRmResourceGroup -Name $newResourceGroup -Location $location
}
else
{
    $newResourceGroup = $json.parameters.resourceGroup.value
	
	Get-AzureRmResourceGroup -Name $newResourceGroup -ErrorVariable resourceGroupNotPresent -ErrorAction SilentlyContinue

	if (!!$resourceGroupNotPresent)
	{
	    #if that resource group does not exists, then create resource group
		New-AzureRmResourceGroup -Name $newResourceGroup -Location $location
	}
}

#################################################################################################################################
##############-------------- AUTOMATION ACCOUNT ----------------##################

Write-Output "************************ EXECUTING AUTOMATION ACCOUNT ****************************"

if ($json.parameters.automationAccount.value -eq '')
{
    #Following block gets last created AUTOMATION ACCOUNT of mentioned pattern and creates the new AUTOMATION ACCOUNT with new number

    Write-Output "Automation Account not found in parameters, creating a new Automation account"
    $lastCreatedAutomationAccount = Get-AzureRmAutomationAccount | Select-Object -ExpandProperty 'AutomationAccountName' | Select-String -Pattern $automationAccountPattern
    
    if(($lastCreatedAutomationAccount.GetType() |Select-Object -ExpandProperty 'Name') -eq  'String')
        {$lastCreatedAutomationAccount = $lastCreatedAutomationAccount.ToString()}
    else
        {$lastCreatedAutomationAccount = ($lastCreatedAutomationAccount| Select-Object -Last 1).ToString()}
    
    $lastCreatedAutomationAccountNumber = [int32]$lastCreatedAutomationAccount.Replace($automationAccountPattern,'')
    $newAutomationAccountNumber = $lastCreatedAutomationAccountNumber + 1
    $newAutomationAccount = $automationAccountPattern + $newAutomationAccountNumber.ToString()  
    
    New-AzureRmAutomationAccount -Name $newAutomationAccount -Location $location -ResourceGroupName $newResourceGroup
}
else
{
    $newAutomationAccount = $json.parameters.automationAccount.value
	
	Get-AzureRmAutomationAccount -Name $newAutomationAccount -ResourceGroupName $newResourceGroup -ErrorVariable automationAccountNotPresent -ErrorAction SilentlyContinue
	if (!!$automationAccountNotPresent)
	{
	    #If that resource not found, then create automation account
		New-AzureRmAutomationAccount -Name $newAutomationAccount -Location $location -ResourceGroupName $newResourceGroup
	}
}

##################################################################################################################################
##############-------------- STORAGE ACCOUNT ----------------##################

Write-Output "************************ EXECUTING STORAGE ACCOUNT ****************************"

if ($json.parameters.storageAccountName.value -eq '')
{
    #Following block gets last created STORAGE ACCOUNT of mentioned pattern and creates the new STORAGE ACCOUNT with new number

    Write-Output "Automation Account not found in parameters, creating a new Automation account"
    $lastCreatedStorageAccount = Get-AzureRmStorageAccount | Select-Object -ExpandProperty 'StorageAccountName' | Select-String -Pattern $storageAccountPattern
    
    if(($lastCreatedStorageAccount.GetType() |Select-Object -ExpandProperty 'Name') -eq  'String')
        {$lastCreatedStorageAccount = $lastCreatedStorageAccount.ToString()}
    else
        {$lastCreatedStorageAccount = ($lastCreatedStorageAccount| Select-Object -Last 1).ToString()}
    
    $lastCreatedStorageAccountNumber = [int32]$lastCreatedStorageAccount.Replace($storageAccountPattern,'')
    $newStorageAccountNumber = $lastCreatedStorageAccountNumber + 1
    $newStorageAccount = $storageAccountPattern + $newStorageAccountNumber.ToString()
        
    New-AzureRmStorageAccount -ResourceGroupName $newResourceGroup -AccountName $newStorageAccount -Location $location -SkuName "Standard_GRS"

}
else
{
    $newStorageAccount = $json.parameters.storageAccountName.value

	Get-AzureRmStorageAccount -ResourceGroupName $newResourceGroup -Name $newStorageAccount -ErrorVariable storageAccountNotPresent -ErrorAction SilentlyContinue	
	if(!!$storageAccountNotPresent)
	{
		#If that resource not found, than create storage account
		 New-AzureRmStorageAccount -ResourceGroupName $newResourceGroup -AccountName $newStorageAccount -Location $location -SkuName "Standard_GRS"
	}
	
}

#####################################################################################################################################################
##############--------------SETTING STORAGE ACCOUNT AND VM CREDENTIALS INTO AUTOMATION ACCOUNT----------------##################

Write-Output "************************ EXECUTING STORAGE ACCOUNT CREDENTIALS INTO AUTOMATION ACCOUNT ****************************"

$storageAccountKey = Get-AzureRmStorageAccountKey -ResourceGroupName $newResourceGroup -AccountName $newStorageAccount

Get-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "StorageAccountCredential" -ResourceGroupName $newResourceGroup -ErrorVariable storageAccountCredentialPresent -ErrorAction SilentlyContinue
if(!$storageAccountCredentialPresent)
{
	Remove-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "StorageAccountCredential" -ResourceGroupName $newResourceGroup
}

sleep(60)
    #Saving Storage account Registration info into Azure Automation credentials
    $User = $newStorageAccount.Insert(0,".\" )
    $Password = ConvertTo-SecureString $storageAccountKey.GetValue(0) -AsPlainText -Force
    $Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $Password
    New-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "StorageAccountCredential" -Value $Credential -ResourceGroupName $newResourceGroup

Write-Output "************************ EXECUTING VIRTUAL MACHINE CREDENTIALS INTO AUTOMATION ACCOUNT ****************************"

Get-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "virtualMachineCredential" -ResourceGroupName $newResourceGroup -ErrorVariable virtualMachineCredentialPresent -ErrorAction SilentlyContinue
if(!$virtualMachineCredentialPresent)
{
	Remove-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "virtualMachineCredential" -ResourceGroupName $newResourceGroup
}

sleep(60)
    #Saving Virtual Machine Credential into Azure Automation credentials
    $User = $json.parameters.serverName.value + "\" + $json.parameters.adminUserName.value
    $Password = ConvertTo-SecureString $json.parameters.adminPassword -AsPlainText -Force
    $Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $Password
    New-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "virtualMachineCredential" -Value $Credential -ResourceGroupName $newResourceGroup
	
#########################################################################################################################################################################################################################################################################
####################################----- UPLOAD MODULES AND APPLICATIONS TO BLOB STORAGE -----#####################################

Write-Output "************************ EXECUTING UPLOAD MODULES TO BLOB STORAGE ****************************"

$ContainerName = "modules"
$storageContext = (Get-AzureRmStorageAccount -ResourceGroupName $newResourceGroup -Name $newStorageAccount).Context

Get-AzureStorageContainer -Name $ContainerName -Context $storageContext -ErrorVariable storageContainerPresent -ErrorAction SilentlyContinue
if(!$storageContainerPresent)
{
    Remove-AzureStorageContainer -Name $ContainerName -Context $storageContext -Force
	sleep(60)
}

New-AzureStorageContainer -Name $ContainerName -Context $storageContext -Permission Container
Set-AzureStorageBlobContent -File $cSqlPath -Container $ContainerName -Blob 'cSql.zip' -Context $storageContext
Set-AzureStorageBlobContent -File $xNetworkingPath -Container $ContainerName -Blob 'xNetworking.zip' -Context $storageContext
Set-AzureStorageBlobContent -File $xPSDesiredStateConfigurationPath -Container $ContainerName -Blob 'xPSDesiredStateConfiguration.zip' -Context $storageContext

Write-Output "************************ EXECUTING UPLOAD APPLICATIONS TO BLOB STORAGE ****************************"

$ContainerName = "applications"

Get-AzureStorageContainer -Name $ContainerName -Context $storageContext -ErrorVariable storageContainerPresent -ErrorAction SilentlyContinue
if(!$storageContainerPresent)
{
    Remove-AzureStorageContainer -Name $ContainerName -Context $storageContext -Force
	sleep(60)
}

New-AzureStorageContainer -Name $ContainerName -Context $storageContext -Permission Container
Set-AzureStorageBlobContent -File $urlShortnerPath -Container $ContainerName -Blob 'Weir.Synertrex.UrlShortener.zip' -Context $storageContext

#####################################################################################################################################################
################------- SETTING APPLICATION's SAS INTO AUTOMATION ACCOUNT CREDENTIALS ---------##################

Write-Output "************************ EXECUTING APPLICATIONS CONTAINER SAS INTO AUTOMATION ACCOUNT ****************************"

$expiryTime = (get-date).AddYears(2)
$permission = "rl"
New-AzureStorageContainerStoredAccessPolicy -Context $storageContext -Container "applications" -Policy "download" -ExpiryTime $expiryTime -Permission $permission
$sasToken = New-AzureStorageContainerSASToken  -Name "applications" -Policy "download" -Context $storageContext

Get-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "containerSharedAccessSig" -ResourceGroupName $newResourceGroup -ErrorVariable containerSharedAccessPresent -ErrorAction SilentlyContinue
if(!$containerSharedAccessPresent)
{
	Remove-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "containerSharedAccessSig" -ResourceGroupName $newResourceGroup
}

sleep(60)
#Saving SAS into Azure Automation credentials
$User = "SharedSignatureKey"
$Password = ConvertTo-SecureString $sasToken -AsPlainText -Force
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $Password
New-AzureRmAutomationCredential -AutomationAccountName $newAutomationAccount -Name "containerSharedAccessSig" -Value $Credential -ResourceGroupName $newResourceGroup

#########################################################################################################################################################################################################################################################################
###############--------------- ADDING REQUIRED MODULES TO AUTOMATION ACCOUNT -------------#########################

Write-Output "************************ EXECUTING ADDING REQUIRED MODULES TO AUTOMATION ACCOUNT ****************************"

$cSqlLinkUri = "https://" + $newStorageAccount + ".blob.core.windows.net/modules/cSql.zip"
$xNetworkingLinkUri = "https://" + $newStorageAccount + ".blob.core.windows.net/modules/xNetworking.zip"
$xPSDesiredStateConfigurationLinkUri = "https://" + $newStorageAccount + ".blob.core.windows.net/modules/xPSDesiredStateConfiguration.zip"

New-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "cSql" -ContentLinkUri $cSqlLinkUri -ResourceGroupName $newResourceGroup
New-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "xNetworking" -ContentLinkUri $xNetworkingLinkUri -ResourceGroupName $newResourceGroup
New-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "xPSDesiredStateConfiguration" -ContentLinkUri $xPSDesiredStateConfigurationLinkUri -ResourceGroupName $newResourceGroup

$moduleProvisioning = Get-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "cSql" -ResourceGroupName $newResourceGroup
while($moduleProvisioning.ProvisioningState –eq "Creating")
{
    $moduleProvisioning = Get-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "cSql" -ResourceGroupName $newResourceGroup
    Start-Sleep -Seconds 3
}

$moduleProvisioning = Get-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "xNetworking" -ResourceGroupName $newResourceGroup
while($moduleProvisioning.ProvisioningState –eq "Creating")
{
    $moduleProvisioning = Get-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "xNetworking" -ResourceGroupName $newResourceGroup
    Start-Sleep -Seconds 3
}

$moduleProvisioning = Get-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "xPSDesiredStateConfiguration" -ResourceGroupName $newResourceGroup
while($moduleProvisioning.ProvisioningState –eq "Creating")
{
    $moduleProvisioning = Get-AzureRmAutomationModule -AutomationAccountName $newAutomationAccount -Name "xPSDesiredStateConfiguration" -ResourceGroupName $newResourceGroup
    Start-Sleep -Seconds 3
}

#########################################################################################################################################################################################################################################################################
###################################------------ UPLOAD AND COMPILE DSC SCRIPT --------------############################

Write-Output "************************ EXECUTING UPLOAD AND COMPILE DSC SCRIPT ****************************"

Get-AzureRmAutomationDscConfiguration -Name "Ssrs" -AutomationAccountName $newAutomationAccount -ResourceGroupName $newResourceGroup -ErrorVariable dscConfigurationPresent -ErrorAction SilentlyContinue
if(!$dscConfigurationPresent)
{
    Remove-AzureRmAutomationDscConfiguration -Name "Ssrs" -AutomationAccountName $newAutomationAccount -ResourceGroupName $newResourceGroup -Force
	sleep(30)
}

Import-AzureRmAutomationDscConfiguration -AutomationAccountName $newAutomationAccount -ResourceGroupName $newResourceGroup -SourcePath $SsrsPath -Force -Published    

 $MyData =@{
    AllNodes = @(
        @{
            NodeName="*"
            PSDscAllowPlainTextPassword=$true
			PsDscAllowDomainUser=$true
         }
        @{
            NodeName="generic"
            Role = "Reporting"
         }
   );
 }

$CompilationJob = Start-AzureRmAutomationDscCompilationJob -ConfigurationName "Ssrs" -ResourceGroupName $newResourceGroup -AutomationAccountName $newAutomationAccount -ConfigurationData $MyData

while($CompilationJob.EndTime –eq $null -and $CompilationJob.Exception –eq $null)
{
    $CompilationJob = $CompilationJob | Get-AzureRmAutomationDscCompilationJob
    Start-Sleep -Seconds 3
}

#########################################################################################################################################################################################################################################################################
##############-------------- AUTOMATION ACCOUNT END POINTS ----------------##################

Write-Output "************************ EXECUTING AUTOMATION ACCOUNT END POINTS ****************************"

$RegistrationInfo = Get-AzureRmAutomationRegistrationInfo -AutomationAccountName $newAutomationAccount -ResourceGroupName $newResourceGroup

$registrationKey = $RegistrationInfo.PrimaryKey
$registrationUrl = $RegistrationInfo.Endpoint


#########################################################################################################################################################################################################################################################################
##############-------------- VIRTUAL MACHINE & DSC Extention Deployment ----------------##################

Write-Output "************************ EXECUTING VIRTUAL MACHINE & DSC Extention Deployment ****************************"

if ($json.parameters.serverName.value -eq '')
{
    #Following block gets last created VIRTUAL MACHINE of mentioned pattern and creates the new VIRTUAL MACHINE number

    Write-Output "Server Name not found in parameters, creating a new Server Virtual Machine number"
    $lastCreatedVmName = Get-AzureRmVM | Select-Object -ExpandProperty 'Name' | Select-String -Pattern $VmNamePattern
    
    if(($lastCreatedVmName.GetType() |Select-Object -ExpandProperty 'Name') -eq  'String')
        {$lastCreatedVmName = $lastCreatedVmName.ToString()}
    else
        {$lastCreatedVmName = ($lastCreatedVmName| Select-Object -Last 1).ToString()}
    
    $lastCreatedVmNameNumber = [int32]$lastCreatedVmName.Replace($VmNamePattern,'')
    $newVmNameNumber = $lastCreatedVmNameNumber + 1
    $newVmName = $VmNamePattern + $newVmNameNumber.ToString()
}
else
{
    $newVmName = $json.parameters.serverName.value
}

Write-Output "************************ PAREMETERS FOR DEPLOYMENT ****************************"

$json.parameters | Add-Member -Name 'nodeConfigurationName' -Type NoteProperty $false
$json.parameters | Add-Member -Name 'registrationKey' -Type NoteProperty $false
$json.parameters | Add-Member -Name 'registrationUrl' -Type NoteProperty $false

$json.parameters.nodeConfigurationName = [PSCustomObject]@{value="Ssrs.generic"} 
$json.parameters.registrationKey = [PSCustomObject]@{value=$registrationKey} 
$json.parameters.registrationUrl = [PSCustomObject]@{value=$registrationUrl} 
$json.parameters.serverName = [PSCustomObject]@{value=$newVmName} 
$json.parameters.storageAccountName = [PSCustomObject]@{value=$newStorageAccount}  
$json.parameters.resourceGroup = [PSCustomObject]@{value=$newResourceGroup}  
$json.parameters.automationAccount = [PSCustomObject]@{value=$newAutomationAccount}  

write-output $json.parameters

$newJson = $json
$newJson.parameters = $json.parameters | Select-Object * -ExcludeProperty @("resourceGroup", "automationAccount")

$newJson | ConvertTo-Json | Set-Content $parametersPath -Force

Write-Output "************************ DEPLOYMENT STARTED ****************************"

New-AzureRmResourceGroupDeployment -ResourceGroupName $newResourceGroup -Name "mydeployment" -TemplateFile $templatePath -TemplateParameterFile $parametersPath

##################################################################################################################################################################
