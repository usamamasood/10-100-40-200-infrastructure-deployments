Configuration Ssrs
{

    Import-DscResource –ModuleName PSDesiredStateConfiguration
    Import-DscResource –ModuleName cSql
	Import-DSCResource -ModuleName xNetworking
	Import-DSCResource -ModuleName xPSDesiredStateConfiguration
	
	$vmCredentials = Get-AutomationPSCredential -Name "virtualMachineCredential"
	$sasTokenCredentials = Get-AutomationPSCredential -Name "containerSharedAccessSig"
	$sasToken = $sasTokenCredentials.GetNetworkCredential().Password
    $StorageAccountCredential = Get-AutomationPSCredential -Name "StorageAccountCredential"
    $storageAccountName = $StorageAccountCredential.UserName.Replace(".\", "")
	
	Write-Host $sasToken

    Node $AllNodes.Where{$_.Role -eq “Reporting”}.NodeName
    {

        $ReportingServicesServiceAccount = New-Object System.Management.Automation.PSCredential("NT Service\ReportServer", (ConvertTo-SecureString "dummy" -AsPlainText -Force))

         User ShadowShareUser
         {
             Ensure = "Present"
             UserName = $storageAccountName
             Password = $StorageAccountCredential
             PasswordChangeNotAllowed = $true
             PasswordNeverExpires = $true
         }
         
         Script SqlServerAgent
         {
             GetScript = {
                 $service = Get-Service SqlServerAgent
                 return @{Result = $service.Status}
                 }
             TestScript = {
                 $service = Get-Service SqlServerAgent
                 $startMode = (Get-WmiObject -Query "Select StartMode From Win32_Service Where Name='sqlserveragent'").StartMode
                 return $service.Status -eq "Running" -and $startMode -eq "Auto"
                 }
             SetScript = {
                 $service = Get-Service SqlServerAgent
                 $startMode = (Get-WmiObject -Query "Select StartMode From Win32_Service Where Name='sqlserveragent'").StartMode

                 if ($startMode -ne "Automatic")
                 {
                     Set-Service SqlServerAgent -StartupType Automatic
                 }

                 if ($service.Status -ne "Running")
                 {
                    Start-Service SqlServerAgent
                 }
             }
         }

        cSql2016ReportingServices InstallSqlServer2016ReportingServices
        {
            InstanceName = "MSSQLSERVER"
            ServiceAccountName = $ReportingServicesServiceAccount.UserName
            Ensure = "Present"
            Mode = "Native"
            ServiceAccountPassword = $ReportingServicesServiceAccount
            LogPath = "C:\SqlSetupLogs\"
            FileShareAccount = $storageAccountCredential
        }
    
        xFirewall Firewall
        {
            Name                  = 'SSRS Port 80'
            DisplayName           = 'Firewall Rule for SSRS Port 80'
            Ensure                = 'Present'
            Enabled               = 'True'
            Profile               = 'Any'
            Direction             = 'InBound'
            Protocol              = 'TCP'
            Description           = 'Firewall Rule for SSRS port 80'
			Action                = 'Allow'
			LocalPort			  = ('80')
        }

		xRemoteFile ApplicationDownload 
		{  			
			Uri             = "https://"+ $storageAccountName +".blob.core.windows.net/applications/Weir.Synertrex.UrlShortener.zip"+ $sasToken 
			DestinationPath = "C:\Weir.Synertrex.UrlShortener.zip"			
		}

		Archive ArchiveExample 
		{
			Ensure = "Present"  
			Path = "C:\Weir.Synertrex.UrlShortener.zip"
			Destination = "C:\"
		}

		Script SettingScheduler
		{
		    SetScript = 
		    { 
		        $Time = New-ScheduledTaskTrigger -Daily -At 23:30
				$action = New-ScheduledTaskAction -Execute "C:\Weir.Synertrex.UrlShortener.exe"
				$User = $vmCredentials.UserName
				$pass= $vmCredentials.GetNetworkCredential().Password
				Register-ScheduledTask -TaskName "Url Shortner" -Trigger $Time -User $User -Password $pass -Action $action
			}
		    TestScript = 
			{
				$Task = Get-ScheduledTask | Where-Object {$_.TaskName -like "Url Shortner" }
				if($Task -ne $null)
					{return $Task.State -eq "Ready"}
				else
					{return $false}
			}
		    GetScript = 
			{ 
				$Task = Get-ScheduledTask | Where-Object {$_.TaskName -like "Url Shortner" }
				if($Task -ne $null)
					{return $Task.State}
				else
					{return "Does not Exists"}
			}          
		}


				
        LocalConfigurationManager 
        { 
            RebootNodeIfNeeded = $true
        } 
    }
}